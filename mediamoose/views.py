from django.http import HttpResponseRedirect, HttpResponse
 
from django.shortcuts import redirect
  

def index(request):
    
   return redirect('/users/') 
  
