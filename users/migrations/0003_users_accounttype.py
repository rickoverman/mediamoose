# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20141125_1129'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='accounttype',
            field=models.CharField(default=b'supplier', max_length=200),
            preserve_default=True,
        ),
    ]
