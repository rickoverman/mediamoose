from django import forms


class LoginForm(forms.Form):
    email = forms.CharField(max_length=200)
    email.widget = forms.TextInput(attrs={'class':'form-control'})
    password = forms.CharField(max_length=200)
    password.widget = forms.PasswordInput(attrs={'class':'form-control'})


class ForgetForm(forms.Form):
    email = forms.CharField(max_length=200)
    email.widget = forms.TextInput(attrs={'class':'form-control'})
    

class SupplierForm(forms.Form):
    name = forms.CharField(max_length=200)
    name.widget = forms.TextInput(attrs={'class':'form-control'})
    address = forms.CharField(max_length=200)
    address.widget = forms.TextInput(attrs={'class':'form-control'})
    tel = forms.CharField(max_length=200)
    tel.widget = forms.TextInput(attrs={'class':'form-control'})
    email = forms.CharField(max_length=200)
    email.widget = forms.TextInput(attrs={'class':'form-control'})
    website = forms.CharField(max_length=200)
    website.widget = forms.TextInput(attrs={'class':'form-control'})
    description = forms.CharField(max_length=200,widget=forms.Textarea)
    description.widget = widget=forms.Textarea(attrs={'class':'form-control'})
    #password = forms.CharField(max_length=200, required=False)
    #password.widget = forms.TextInput(attrs={'class':'form-control','disabled':'disabled'})
    avatar = forms.ImageField(required=False);   


class ConsumerForm(forms.Form):
    name = forms.CharField(max_length=200)
    name.widget = forms.TextInput(attrs={'class':'form-control'})
    address = forms.CharField(max_length=200,required=False)
    address.widget = forms.TextInput(attrs={'class':'form-control'})
    tel = forms.CharField(max_length=200,required=False)
    tel.widget = forms.TextInput(attrs={'class':'form-control'})
    email = forms.CharField(max_length=200)
    email.widget = forms.TextInput(attrs={'class':'form-control'})
    #password = forms.CharField(max_length=200, required=False)
    #password.widget = forms.TextInput(attrs={'class':'form-control','disabled':'disabled'})














