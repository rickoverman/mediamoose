from django.conf.urls import patterns, url
from users import views
from django.conf import settings


urlpatterns = patterns('',

    url(r'^$', views.index, name='index'),
    url(r'^forget/$', views.forget, name='forget'),
    url(r'^detail/$', views.detail, name='detail'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^resetpassword/$', views.resetpassword, name='logout'),
    url(r'^play/$', views.play, name='play'),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

 

)
