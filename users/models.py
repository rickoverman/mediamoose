from django.db import models


class Users(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    tel = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    website = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    accounttype = models.CharField(max_length=200,default='supplier')
    avatar = models.ImageField(upload_to='avatar/')
    #pub_date = models.DateTimeField('date published')


