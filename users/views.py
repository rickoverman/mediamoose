from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.shortcuts import redirect, render_to_response, get_object_or_404, render
from django.forms import CharField, Form, PasswordInput
from django.forms.models import model_to_dict, modelformset_factory
from django.contrib import messages
from django.contrib.messages import get_messages

from users import forms
from users.models import Users

import hashlib


def index(request):
    ''' main '''
    if request.method == 'POST':
        
        loginform = forms.LoginForm(request.POST)
        if loginform.is_valid():
            try:
                 
                shapassword = hashlib.sha1(request.POST['password']).hexdigest()

                user = Users.objects.get(email=request.POST['email'],password=shapassword)
                if user:
                    
                    request.session['userpk'] = user.pk
                    return redirect('/users/detail/')  
                           
            except Users.DoesNotExist:
                messages.success(request, 'User was not found!')
            
    else:
        loginform = forms.LoginForm()

    flash_messages = messages.get_messages(request)

    return render(request, 'login.html',{'loginform':loginform,'flash_messages':flash_messages}) 
 


def detail(request):
    
    if not request.session['userpk']:
        return redirect('/users/') 

    user = Users.objects.get(pk=request.session['userpk'])

    if user.accounttype == 'supplier':
        if request.method == 'POST':
            form = forms.SupplierForm(request.POST, request.FILES)
            if form.is_valid():
                user.name = request.POST['name']
                user.address = request.POST['address']
                user.tel = request.POST['tel']
                user.email = request.POST['email']
                user.website =  request.POST['website']
                user.description = request.POST['description']
                if(request.FILES):
                    user.avatar = request.FILES['avatar']
                user.save()
                
                messages.success(request,'User data has been updated!' )

        else:
            form = forms.SupplierForm(initial=model_to_dict(user))
    else:
        if request.method == 'POST':
            form = forms.ConsumerForm(request.POST)
            if form.is_valid():
                user.name = request.POST['name']
                user.address = request.POST['address']
                user.tel = request.POST['tel']
                user.email = request.POST['email']
                user.save()
                messages.success(request,'User data has been updated!' )
        else:
            form = forms.ConsumerForm(initial=model_to_dict(user))
    
    user = Users.objects.get(pk=request.session['userpk'])

    flash_messages = messages.get_messages(request)

    return render(request, 'detail.html',{'user':user,'form':form,'flash_messages':flash_messages}) 
    

def logout(request):
    request.session['userpk'] = ''
    return redirect('/users/') 


def resetpassword(request):
    if not request.session['userpk']:
        return redirect('/users/') 
     
    user = Users.objects.get(pk=request.session['userpk'])
                     
    user.password = hashlib.sha1('1234').hexdigest()
    user.save()

    send_mail('Mediamoose password reset', 'Your password has been reset to 1234', 'info@mediamoose.nl',[user.email], fail_silently=False)

    action_message  = 'Your new pasword has been send to ' + user.email  

    messages.success(request, action_message)

    return redirect('/users/detail') 


def forget(request):
    
    if request.method == 'POST':
        form = forms.ForgetForm(request.POST)
        if form.is_valid():
            try:
                    
                user = Users.objects.get(email=request.POST['email'])
                if user:       
                    
                    user.password = hashlib.sha1('1234').hexdigest()
                    user.save()

                    send_mail('Mediamoose password reset', 'Your password has been reset to 1234', 'info@mediamoose.nl',[user.email], fail_silently=False)
                    messages.success(request, 'Your new pasword has been send to ' + user.email  )
           
            except Users.DoesNotExist:
                messages.success(request, 'User was not found!')
            
    else:
        form = forms.ForgetForm()
    
    flash_messages = messages.get_messages(request)

    return render(request,'forget.html',{'form':form,'flash_messages':flash_messages})


def play(request):

    return HttpResponse('Check the console')

